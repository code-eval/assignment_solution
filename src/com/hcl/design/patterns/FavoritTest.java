package com.hcl.design.patterns;

public class FavoritTest {

	// @Test
	public void shouldShowMovies() {
		// fail case not implemented.
	}

	public void showMoviesWithWrongDetails() {
		// fail case with wrong output.
	}

	public void ShouldShowOneMovie() {
		// test for one object
	}

	public void shouldShowAllMovies() {
		// Showing all movies from file
		Favorite favorite = new Favorite();
		favorite.showMovies();
	}

	public void shouldAddToFavorite() {
		// fail case not implemented
	}

	public void addToFavoriteWrongDetails() {
		// Adding wrong id and title to file
	}

	public void addOneMovie() {
		// Adding one movie to file
	}

	public void shouldAddMultipleMovies() {
		// Adding multiple movies to file
		Favorite favorite = new Favorite();
		favorite.addToFavorite();
	}
}
