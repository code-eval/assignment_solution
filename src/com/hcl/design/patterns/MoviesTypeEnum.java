package com.hcl.design.patterns;

public enum MoviesTypeEnum { // sigleton design pattern

	COMING_SOON("ComingSoon"), MOVIES_IN_THEATER("MoviesInTheater"), TOP_RATED_INDIAN_MOVIES("TopRatedIndianMovies"),
	TOP_RATED_MOVIES("TopRatedMovies"), FAVORITE("Favorite");

	private MoviesTypeEnum(String movies) {

	}

	public String toString() {
		return this.toString();
	}

}
