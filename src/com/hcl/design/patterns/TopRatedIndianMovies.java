package com.hcl.design.patterns;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Scanner;
import com.hcl.utils.CustomScanner;

public class TopRatedIndianMovies implements Movies {

	Scanner sc = CustomScanner.getScanner();

	@Override
	public void showMovies() {

		JSONParser parser = new JSONParser();

		try {

			Object obj = parser.parse(new FileReader("src\\com\\hcl\\json\\data.json"));
			JSONObject jsonObject = (JSONObject) obj;

			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
			System.out.println("Movies Are :");
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

			JSONArray listOfComingSoon = (JSONArray) jsonObject.get("top-rated-india");
			@SuppressWarnings("rawtypes")
			Iterator iterator = listOfComingSoon.iterator();

			while (iterator.hasNext()) {

				System.out.println(iterator.next());

			}
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} catch (ParseException e) {

			e.printStackTrace();

		}

		System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
		System.out.println("Press 1 For Add \nPress 2 For Exit");
		System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

		int ch = sc.nextInt();
		switch (ch) {

		case 1:

			addToFavorite();
			break;

		case 2:

			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
			System.out.println("Returning to home...");
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
			break;

		default:

			System.out.println("Enter right Choice");

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void addToFavorite() {

		System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
		System.out.println("Enter Number Of Movies You Want To Add:");
		int n = sc.nextInt();
		System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

		String[] title = new String[n];
		JSONObject obj = new JSONObject();
		String[] id = new String[n];

		for (int i = 0; i < n; i++) {

			System.out.println("Enter the movie id" + (i + 1) + " and movie title" + (i + 1));
			id[i] = sc.next();
			title[i] = sc.next();
			obj.put(id[i], title[i]);
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

			try (FileWriter file = new FileWriter("src\\com\\hcl\\json\\favorite.json")) {

				file.write(obj.toJSONString());

			} catch (IOException e) {

				e.printStackTrace();

			}

			System.out.println();
			System.out.println(obj);
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

		}
	}
}