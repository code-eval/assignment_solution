package com.hcl.design.patterns;

public interface Movies {

	public void showMovies();

	public void addToFavorite();

}
