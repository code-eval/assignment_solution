package com.hcl.design.patterns;

public class MoviesFactory {

	public static Movies getMovies(MoviesTypeEnum moviesType) { // factory method pattern

		if (moviesType.equals(MoviesTypeEnum.COMING_SOON)) {

			return new ComingSoon();

		} else if (moviesType.equals(MoviesTypeEnum.MOVIES_IN_THEATER)) {

			return new MoviesInTheater();

		} else if (moviesType.equals(MoviesTypeEnum.TOP_RATED_INDIAN_MOVIES)) {

			return new TopRatedIndianMovies();

		} else if (moviesType.equals(MoviesTypeEnum.TOP_RATED_MOVIES)) {

			return new TopRatedMovies();

		} else if (moviesType.equals(MoviesTypeEnum.FAVORITE)) {

			return new Favorite();

		}

		return null;

	}
}
