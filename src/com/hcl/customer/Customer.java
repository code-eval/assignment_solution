package com.hcl.customer;

import com.hcl.design.patterns.*;
import java.util.Scanner;
import com.hcl.utils.CustomScanner;

public class Customer {

	public static void main(String[] args) {

		boolean loop = true;
		Scanner sc = CustomScanner.getScanner();

		while (loop) {

			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
			System.out.println("Welcome To Movies On Tips");
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

			System.out.println(
					"1.Press 1 For Coming Soon\n2.Press 2 For Movies In Theater\n3.Press 3 For Top Rated Indian Movies\n4.Press 4 For Top Rated Movies\n5.Press 5 For Favorite\n6.Press 0 For Exite");
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

			System.out.println("Enter Your Choice:");
			int ch = sc.nextInt();
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

			switch (ch) {

			case 1:

				Movies moviesComingSoon = MoviesFactory.getMovies(MoviesTypeEnum.COMING_SOON);
				moviesComingSoon.showMovies();
				break;

			case 2:

				Movies moviesMoviesInTheater = MoviesFactory.getMovies(MoviesTypeEnum.MOVIES_IN_THEATER);
				moviesMoviesInTheater.showMovies();
				break;

			case 3:

				Movies moviesTopRatedIndianMovies = MoviesFactory.getMovies(MoviesTypeEnum.TOP_RATED_INDIAN_MOVIES);
				moviesTopRatedIndianMovies.showMovies();
				break;

			case 4:

				Movies moviesTopRatedMovies = MoviesFactory.getMovies(MoviesTypeEnum.TOP_RATED_MOVIES);
				moviesTopRatedMovies.showMovies();
				break;

			case 5:

				Movies moviesFavorite = MoviesFactory.getMovies(MoviesTypeEnum.FAVORITE);
				moviesFavorite.showMovies();
				break;

			case 0:

				System.out.println("Logout...");
				loop = false;
				break;

			default:

				System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
				System.out.println("Enter right Choice");
				System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

			}
			continue;
		}
	}

}
